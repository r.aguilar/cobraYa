<?php

namespace CompraYa\Models\Product;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    //
    protected $table = 'products';
    protected $primaryKey = 'id';
    protected $fillable = [
      'id','name','price','marks_id'
    ];

    public function mark()
    {
      // hasmany -> Que un producto puede tener una o muchas marcas
      return $this->hasmany(Mark::class);
    }
}
