<?php

namespace CompraYa\Models\Product;

use Illuminate\Database\Eloquent\Model;

class Mark extends Model
{
    //
    protected $table = 'marks';
    protected $primaryKey = 'id';
    protected $fillable = [
      'id','name'
    ];

    public function product()
    {
      // belongsto -> Que pertenece a producto
      return $this->belongsto(Product::class);
    }
}
