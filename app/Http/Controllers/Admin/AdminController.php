<?php

namespace CompraYa\Http\Controllers\Admin;

use Illuminate\Http\Request;
use CompraYa\Http\Controllers\Controller;

class AdminController extends Controller
{
    //
    public function panel()
    {
      return view('admin/panel');
    }

    public function access()
    {
      return view('admin/access');
    }

    public function reports()
    {
      return view('admin/reports');
    }
}
